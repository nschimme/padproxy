You need Python installed.

1. Run `python padproxy.py`.
2. Set your device to [use a proxy](http://www.pocketables.com/2012/05/how-to-setup-a-proxy-server-on-your-tablet-apple-android-2.html).
   Use the IP address of the machine you are running the proxy on and 8000 for port.
3. Load PAD, go past start screen.
4. Wait until proxy says `-- CAPTURED BOX DATA --`.
5. Close PAD.
6. Kill proxy (Ctrl-C).
7. Disable proxy on your device.

You now have a capture.something file with your box data.
